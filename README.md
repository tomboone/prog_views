# Programmatic Views #

Stock module for enabling storage of exported Views in Drupal codebase instead of database. Views export files should be placed in 'views' folder with 'view_machine_name.inc' naming convention.

Based on the Easy Method described at:
https://www.drupal.org/node/1014774